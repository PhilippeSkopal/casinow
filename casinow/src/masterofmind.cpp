#include "masterofmind.h"
#include <iostream>
#include <stdlib.h>

masterofmind::masterofmind()
{
    //ctor
    nbEssais = 10;
    partieGagnee = false;

    std::cout << "[?][?][?][?]" << std::endl;

    for (int i=0; i<4; ++i) {
        combiSecrete[i] = rand()%10;

        // sinon c'est trop easy :)
        //std::cout << combiSecrete[i];
    }
    std::cout << std::endl;
}

masterofmind::~masterofmind()
{
    //dtor
}

int * masterofmind::getCombiSecrete() {
    return combiSecrete;
}

void masterofmind::demandeCombiJoueur() {
    for (int i=0; i<4; ++i) {
        std::cout << "Chiffre " << (i+1) << " : ";
        std::cin >> combiJoueur[i];
        std::cout << std::endl;
    }
}

bool masterofmind::jouer() {
    while (nbEssais && !partieGagnee)
    {
        std::cout << nbEssais << " essais restants." << std::endl;
        //On continue � jouer tant qu'on a des essais et que la partie n'est pas gagnee
        //Le joueur entre les 4 chiffres
        demandeCombiJoueur();
        std::cout << "Votre combinaison est: " << combiJoueur[0] << " " << combiJoueur[1] << " " << combiJoueur[2] << " " << combiJoueur[3] << std::endl;

        //Test de la combinaison
        int nbBienPlace=0;
        int nbMalPlace=0;
        for (int i=0; i<4; ++i)
        {
            if (combiJoueur[i] == combiSecrete[i])
            {
                //Bien place
                nbBienPlace++;
            }
            else
            {
                //Test mal place
                for (int j=0; j<4; ++j)
                {
                    if (i!=j)
                    {
                        //On ne reteste pas le bien place
                        if (combiJoueur[i] == combiSecrete[j])
                        {
                            nbMalPlace++;
                            break; //On evite de compter plusieurs fois
                        }
                    }
                } //fin boucle
            } //fin mal place
        }
        std::cout << nbBienPlace << " bien places" << std::endl;
        std::cout << nbMalPlace << " mal places" << std::endl;

        if (nbBienPlace==4)
        {
            partieGagnee = true;
        }

        //Retirer un essai
        nbEssais--;
    }

    return partieGagnee;
}
