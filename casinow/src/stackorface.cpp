#include "stackorface.h"
#include <iostream>
#include <stdlib.h>

stackorface::stackorface()
{
    //ctor
    SetPlayerScore(0);
    SetComputerScore(0);
}

stackorface::~stackorface()
{
    //dtor
}

int stackorface::GetComputerScore(){
    return lComputerScore;
}
int stackorface::GetPlayerScore(){
    return lPlayerScore;
}
void stackorface::SetComputerScore(int Value){
    lComputerScore = Value;
}
void stackorface::SetPlayerScore(int Value){
    lPlayerScore = Value;
}

bool stackorface::jouer() {

    while (GetPlayerScore() <3 && GetComputerScore() <3) {

        //Tour humain
        tour(false);

        //Tour ordi
        tour(true);

        //Affiche score
        std::cout << "Score joueur " << lPlayerScore << " - Score ordi " << lComputerScore << std::endl;

    }
     // return le bool de win ou pas
    if (GetPlayerScore() == 3) {
            return true;
    }else {
        return false;
    }
}

void stackorface::tour(bool choixAuto) {
    //Choix pile/face
    int choix = 0;
    int lancer = 0;

    if (choixAuto) {
        std::cout << "Tour Ordi" << std::endl;
        choix = rand()%2+1;
    } else {
        std::cout << "Tour Humain" << std::endl;
        std::cout << "Pile = 1 - Face = 2" << std::endl << "Choix : ";
        std::cin >> choix;
    }
    affichePileFace(choix);

    //Lancer de pi�ce
    lancer = rand()%2+1;

    affichePileFace(lancer);

    //Check gagn�
    if (choix == lancer && choixAuto == true) {
        SetComputerScore(GetComputerScore()+1);
    }else if(choix == lancer && choixAuto == false){
        SetPlayerScore(GetPlayerScore()+1);
    }
}

void stackorface::affichePileFace(int piece) {
    switch (piece) {
        case 1:
            std::cout << "Pile !" << std::endl;
            break;
        case 2:
            std::cout << "Face !" << std::endl;
            break;
    }
}
