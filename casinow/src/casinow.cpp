#include "casinow.h"
#include <masterofmind.h>
#include <rouwlette.h>
#include <stackorface.h>

#include <string>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <Windows.h> // Pour Sleep()

casinow::casinow()
{
    //ctor
    lKeepPlay = true;
}

casinow::~casinow()
{
    //dtor
}

// Boucle principale
void casinow::play(){

    MaitreDuMonde = new player;

    std::string Temp = "";
    int Choice = 0;
    bool win;
    bool ValidChoice = true;
    bool ValidBet = true;
    int Bet = 0;

    while(lKeepPlay){

        ValidChoice = true;

        while(ValidChoice){
            system("cls");
            std::cout << "Choisis un jeu :), il te reste " << MaitreDuMonde->GetNbTokens() << " Jetons."<< std::endl;
            std::cout << "1 - Mastermind" << std::endl;
            std::cout << "2 - Roulette" << std::endl;
            std::cout << "3 - Pile Ou Face" << std::endl;

            std::string Temp;
            std::getline(std::cin, Temp);

            try{
                std::istringstream buffer(Temp);

                buffer >> Choice;
            }catch(...){
                std::cout << "Sale troll c'est pas un chiffre"<< std::endl;
            }
            if (Choice >0 && Choice <= 3 ){ValidChoice = false;}
        }

        while(ValidBet){

            system("cls");

            std::cout << "Entre ta mise : " << std::endl;
            std::cin >> Bet;
            if (Bet > 0 && Bet <= MaitreDuMonde->GetNbTokens() ){
                ValidBet = false;
            }
        }

        switch(Choice){
            case 1:
                {
                    masterofmind * mastermind = new masterofmind();
                    win = mastermind->jouer();
                    delete mastermind;

                    break;

                }

            case 2:
                {
                    rouwlette * Roulette = new rouwlette;
                    win = Roulette->Jouer();
                    delete Roulette;

                    break;

                }
            case 3:
                {
                    stackorface * PileOuFace = new stackorface;
                    win = PileOuFace->jouer();
                    delete PileOuFace;

                    break;

                }
        }

        if (win){
            std::cout << "gagne mise" << std::endl;
            MaitreDuMonde->SetNbTokens(MaitreDuMonde->GetNbTokens()+ Bet);
            Sleep(500);

        }else{
            std::cout << "perd mise" << std::endl;
            MaitreDuMonde->SetNbTokens(MaitreDuMonde->GetNbTokens()- Bet);
            Sleep(500);
        }
        CheckPlayerMoney();

        Temp = "";
        Choice = 0;
        win = false;
    }
}

void casinow::CheckPlayerMoney(){

    if (MaitreDuMonde->GetNbTokens() <= 0){
        lKeepPlay = false;
    }
}
