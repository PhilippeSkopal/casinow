#include "rouwlette.h"
#include <iostream>
#include <Windows.h> // Pour Sleep()
#include <time.h>
#include <string.h>

rouwlette::rouwlette()
{
    //ctor
}

rouwlette::~rouwlette()
{
    //dtor
}

bool rouwlette::Jouer(){

    // #### D�claration des variables ####
    bool ValidBet;
    int PlayerBetNumber = -1;
    int RouletteResult;

    ValidBet = false;                                                                         // R�-initialise le bool�en de contr�le
    while(!ValidBet){                                                                         // Boucle pour r�cup�rer un num�ro sur lequel le joueur mise

        if(PlayerBetNumber >= 0 && PlayerBetNumber < 37 ){                                    // Si nombre entr� sup�rieur/�gal � et inf�rieur � 37, alors
            ValidBet = true;                                                                  // la valeur et valide et la variable de contr�le passe � true

        }else{

            std::cout << "Choisis le num�ro sur lequel tu mise :";
            std::cin >> PlayerBetNumber;                                                            // R�cup�re l'entr�e de l'utilisateur
        }
    }

    RouletteResult = (rand()%37);                                                               // Donne une valeur al�atoire entre 0 et 37 � la variable
    std::cout <<  RouletteResult << std::endl;

    DisplayAllThrow(RouletteResult);                                                            // Lance l'animation
    Sleep(500);                                                                                 // met en pause le programme pour 500 milisecondes

    if(RouletteResult == PlayerBetNumber){                      // si le joueur � mis� le bon nombre, alors
        return true;

    }else{
        return false;
    }
}

void rouwlette::DisplayLoop(int Speed, int BreakPoint)
{
    int i;
    int j;

    for(i=0; i<37; i++){
        system("cls");
        for (j=0; j<i; j++){
            if(i == 0){

            }else{
                std::cout << "-" << j;
            }
        }
        std::cout << "-[" << i << "]";
        for (j=i+1; j<37; j++){
            if(i==36){

            }else{
                std::cout << "-" << j;
            }
        }
        if(i==BreakPoint){
            break;
        }
        Sleep(Speed);
    }
}

// Lance plusieurs fois la fnc suivante et int�gre une valeur d'arr�t pour l'animation de la roulette
void rouwlette::DisplayAllThrow(int BreakPoint /*= -1*/)
{
    DisplayLoop(25, -1);
    DisplayLoop(50, -1);
    DisplayLoop(100,BreakPoint);
}
