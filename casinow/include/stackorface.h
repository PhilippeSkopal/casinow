#ifndef STACKORFACE_H
#define STACKORFACE_H


class stackorface
{
    public:
        stackorface();
        virtual ~stackorface();

       bool jouer();

       int GetPlayerScore();
       void SetPlayerScore(int Value);

       int GetComputerScore();
       void SetComputerScore(int Value);
    protected:

    private:
        void tour(bool choixAuto);
        void affichePileFace(int piece);

        int lPlayerScore;
        int lComputerScore;
};

#endif // STACKORFACE_H
