#ifndef PLAYER_H
#define PLAYER_H


class player
{
    public:
        player();
        virtual ~player();

        int GetNbTokens();
        void SetNbTokens(int Value);
    protected:

    private:
        int lNbTokens;
};

#endif // PLAYER_H
