#ifndef CASINOW_H
#define CASINOW_H

#include <player.h>
#include <masterofmind.h>
#include <rouwlette.h>
#include <stackorface.h>

class casinow
{
    public:
        casinow();
        virtual ~casinow();

        void play(); // meth principale

    protected:

    private:
        player * MaitreDuMonde;

        void CheckPlayerMoney();
        bool lKeepPlay;

};

#endif // CASINOW_H
