#ifndef MASTEROFMIND_H
#define MASTEROFMIND_H


class masterofmind
{
    public:
        masterofmind();
        virtual ~masterofmind();

        void demandeCombiJoueur();
        int * getCombiSecrete();

        bool jouer();
    protected:

    private:
        int combiSecrete[4];
        int combiJoueur[4];
        int nbEssais;
        bool partieGagnee;
};

#endif // MASTEROFMIND_H
